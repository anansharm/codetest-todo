angular.module('todoService', [])

.factory('Todo', function($http){

	var todoFactory = {};

	todoFactory.create = function(todoData){

		return $http.post('/api', todoData); 
	}

	todoFactory.allTodo = function(){
		return $http.get('/api');
	}

	return todoFactory;

})

.factory('socketio', function($rootScope){
	var socket = io.connect();
	return {
		on: function(eventName, callback){
			socket.on(eventName, function(){
				var args = arguments;
				$rootScope.$apply(function(){
					callback.apply(socket, args); 

				});
			});
		},

		emit: function(eventName, data, callback){
			socket.emit(eventName, data, function(){
				var args = arguments;
				$rootScope.apply(function(){
					if (callback) {
						callback.apply(socket, args);
					}
				});
			});
		}
	};
});