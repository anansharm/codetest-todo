angular.module('todoCtrl', ['todoService'])


.controller('todoController', function($scope, Todo, socketio){

	var vm = this;

	Todo.allTodo()
		.success(function(data){

			vm.todos = data;

		});

		vm.createTodo = function(){
			vm.message = '';
			Todo.create(vm.todoData)
				.success(function(data){
					vm.todoData = '';

					vm.message = data.message;

					
				});
		};

		socketio.on('todo', function(data){
			vm.todos.push(data);
		});


})

