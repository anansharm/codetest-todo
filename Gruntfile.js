module.exports = function(grunt){
	
	// Project configuration.
	grunt.initConfig({
	  concat: {
	    // options: {
	    //   separator: ';',
	    // },
	    css: {
	      src: ['public/css/main.css', 'public/css/theme.css'],
	      dest: 'build/css/built.css',
	    },
	    js: {
	      src: ['public/js/main.js', 'public/js/controller.js'],
	      dest: 'build/js/built.js',
	    },
	  },
	  watch: {
		  js: {
		    files: ['public/js/**/*.js'],
		    tasks: ['concat'],
		  },
		  css: {
		    files: ['public/css/**/*.css'],
		    tasks: ['concat'],
		  },
		},
	});

	grunt.loadNpmTasks('grunt-contrib-concat');
	grunt.loadNpmTasks('grunt-contrib-watch');
	grunt.registerTask('default', ['concat', 'watch']);
};