//required files
var express = require('express');
var bodyParser = require('body-parser');
var morgan = require('morgan');

var mongoose = require('mongoose');

var config = require('./config');  //this is the file I created, which will be very useful later on 

var app = express();

//This is for refreshing the page realtime
var http = require('http').Server(app);
var io = require('socket.io')(http);

//connecting to the database
mongoose.connect(config.database, function(err){
	if (err) {
		console.log(err);
	}
	else{
		console.log('We are connected to the mongoLab database');
	}
});

//here comes the middleware
//used extended: true to make sure to get anything like a images later on just in case
app.use(bodyParser.urlencoded({
	extended: true
}));
app.use(bodyParser.json());
app.use(morgan('dev'));

//render all the static files in public folder before you get anywhere
app.use(express.static(__dirname + '/public'));

//Lets test our api
var api = require('./app/routes/api')(app, express, io);
app.use('/api', api);

//any route go to index.html page
app.get('*', function(req, res){
	res.sendFile(__dirname + '/public/app/views/index.html');
});

http.listen(config.port, function(err){
	if (err) {
		console.log(err);
	}
	else{
		console.log('Listening on port 5000');
	}
});