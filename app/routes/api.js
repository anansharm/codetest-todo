var user = require('../models/user');

var config = require('../../config');

var secretKey = config.secretKey;

var jsonwebtoken = require('jsonwebtoken'); //requiring jsonwebtoken to create token for the login api

var Todo = require('../models/todo');

//function for creating token for the login
function createToken(user){
    var token = jsonwebtoken.sign({
        id: user._id,
        name: user.name,
        username: user.username
    }, secretKey, {
        //expirtesInMinute: 1440 //dont know why cant i use this // will need to revisit again

    });
    return token;
}

module.exports = function(app, express, io){
	var api = express.Router();

//api for creating users
	api.post('/signup', function(req, res){
		var users = new user({
			name: req.body.name,  //body is nothing but body parser
			username: req.body.username,
			password: req.body.password
		});
        var token = createToken(users);

		users.save(function(err){
			if(err){
				res.send(err);
				return;
			}
			//if no error do the following
			res.json({ 
                success: true,
                message: 'You have successfully created the user',
                token: token
            }); 
		});
	});

	//api to get all users from the database
	api.get('/users', function(req, res){
		user.find({}, function(err, users){
			if(err){
				res.send(err);
				return;
			}
			//if no errors show all the users
			res.json(users);
		});
	});



	//api for login with the token authentication approach
	api.post('/login', function(req, res){
        user.findOne({
            username: req.body.username
        }).select('name username password').exec(function(err, user){

            if(err) throw err;
            if(!user){
                res.send({message: "User Doesn't Exist"});
            }else if(user){
                var validPassword = user.comparePassword(req.body.password);

                if(!validPassword){
                    res.send({message: "Invalid Password"});
                }
                else{
                    //create a token//
                    var token  = createToken(user);
                    res.json({
                        success: true,
                        message: "Logged In Successfully!",
                        token: token
                    });
                }
            }

        });

    });

    //here comes the middleware
    //this is the check before you go to heaven
    api.use(function(req, res, next){
    	console.log("Yeah! you came to our app");

    	var token = req.body.token || req.param('token') || req.headers['x-access-token'];

    	//check if token exist
    	if(token){ //if the token exist do this
    		jsonwebtoken.verify(token, secretKey, function(err, decoded){
    			if (err) {
    				res.status(403).send({success: false, message: "Failed to authenticate users"});
    			}
    			else
    			{
    				req.decoded = decoded;
    				next();
    			}
    		});
    	}
    	else{  //if token doesnt exist do this
    		res.status(403).send({success: false, message: "Where is your token, Stupid!"});
    	}
    });

    // //after token verification this is heaven
    // api.get('/', function(req, res){
    // 	res.json('You are finally here');
    // });

    //multiple http request on a single route by chaining method
    api.route('/')
    	//chaining post http request
    	.post(function(req, res){
    		var todo = new Todo({
    			creator: req.decoded.id,
    			content: req.body.content,
    			//I want to do somthing here//will comeback later
    			created: req.body.date
    		});

    		todo.save(function(err, newTodo){
    			if (err) {
    				res.send(err);
    				return;
    			}
                io.emit('todo', newTodo);
    			res.json({message: "New Todo created"});
    		});
    	})

    	//chaining get http request
    	.get(function(req, res){
    		Todo.find({ creator: req.decoded.id}, function(err, todos){
    			if (err) {
    				res.send(err);
    				return;
    			}
    			res.json(todos);
    		});
    	});

    	//to send the decoded to the frontend we are creating an api 
    	//otherwise front end cant get to the decoded
    	// api.get('/me', function(req, res){
    	// 	res.json(req.decoded);
    	// });
        api.get('/me', function(req, res){
            res.json(req.decoded);
        })


	return api;
}