var mongoose = require('mongoose');
var bcrypt = require('bcrypt-nodejs');  //for encrypting the password

var Schema = mongoose.Schema; //Schema is a method in moongoose

var users = new Schema({
	name: String,
	username: { type: String, required: true, index: {unique: true}},
	password: { type: String, required: true, selected: false} //no quering the password, so select is false
});


//This is how password are encrypted using bcrypt
users.pre('save', function(next){

	var user = this;

	if(!user.isModified('password')) return next(); //if password is not modified go to next matching route
	bcrypt.hash(user.password,null,null, function(err, hash){
		if(err) return next(err);
		user.password = hash;
		next();
	});
});

//custom method for comparing the password using bcrypt
users.methods.comparePassword = function(password){
	var user = this;
	return bcrypt.compareSync(password, user.password);
}

module.exports = mongoose.model('user', users);